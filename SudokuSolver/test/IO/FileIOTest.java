/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import sudokusolver.BadInputException;
import sudokusolver.IllegalAssignmentException;
import sudokusolver.Puzzle;

/**
 *
 * @author ammon
 */
public class FileIOTest {
    private static String testFileLocation;
    private static Puzzle testPuzzle;
    
    public FileIOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        try {
            testFileLocation = "/home/ammon/usu/ObjectOriented/Homework4/SudokuSolver/SamplePuzzles/";
            String values[] = {"1", "2", "3", "4" };
            testPuzzle = new Puzzle(4, values);
            testPuzzle.setCell(0, 0, "2");
            testPuzzle.setCell(0, 2, "3");
            testPuzzle.setCell(0, 3, "1");
            testPuzzle.setCell(1, 0, "1");
            testPuzzle.setCell(1, 1, "3");
            testPuzzle.setCell(1, 3, "4");
            testPuzzle.setCell(2, 0, "3");
            testPuzzle.setCell(2, 1, "1");
            testPuzzle.setCell(2, 2, "4");
            testPuzzle.setCell(3, 1, "2");
            testPuzzle.setCell(3, 2, "1");
            testPuzzle.setCell(3, 3, "3");
        } catch (BadInputException | IllegalAssignmentException ex) {
            Logger.getLogger(FileIOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of load method, of class FileIO.
     */
    @Test
    public void testLoadSuccess() throws Exception {
        System.out.println("loadSuccess");
        FileIO instance = new FileIO();
        instance.setInFile(testFileLocation + "Input/Puzzle-4x4-valid.txt");
        instance.setOutFile(testFileLocation + "Test/Puzzle-4x4-valid.txt");
        String values[] = {"1", "2", "3", "4" };
        Puzzle expResult = new Puzzle(values.length, values);
        expResult.setCell(0,0,"2");
        expResult.setCell(0,3,"1");
        expResult.setCell(1,0,"1");
        expResult.setCell(1,1,"3");
        expResult.setCell(2,1,"1");
        expResult.setCell(2,2,"4");
        expResult.setCell(3,2,"1");
        expResult.setCell(3,3,"3");
        Puzzle result = instance.load();
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of load method, of class FileIO.
     */
    @Test
    public void testLoadIllegalValue() throws Exception {
        System.out.println("loadIllegalValue");
        FileIO instance = new FileIO();
        instance.setInFile(testFileLocation + "Input/Puzzle-4x4-0903.txt");
        instance.setOutFile(testFileLocation + "Test/Puzzle-4x4-0903.txt");
        Puzzle expResult = null;
        Puzzle result = instance.load();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of load method, of class FileIO
     * Should output bad puzzle issue
     */
    @Test
    public void testLoadFail() throws Exception {
        System.out.println("loadFail");
        FileIO instance = new FileIO();
        instance.setInFile(testFileLocation + "Input/Puzzle-4x4-0901.txt");
        instance.setOutFile(testFileLocation + "Test/Puzzle-4x4-0901.txt");
        Puzzle expResult = null;
        Puzzle result = instance.load();
        assertEquals(expResult, result);
    }

    /**
     * Test of print method, of class FileIO.
     */
    @Test
    public void testPrint() {
        try {
            System.out.println("print");
            FileIO instance = new FileIO();
            instance.setInFile(testFileLocation + "Test/Puzzle-4x4-0001-2.txt");
            instance.setOutFile(testFileLocation + "Test/Puzzle-4x4-0001-2.txt");
            instance.print(testPuzzle);
            Puzzle result = instance.load();
            assertEquals(testPuzzle.toString(), result.toString());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileIOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of setInFile method, of class FileIO.
     */
    @Test
    public void testSetInFile() {
        System.out.println("setInFile");
        String name = "testPuzzle";
        FileIO instance = new FileIO();
        instance.setInFile(name);
        String result = instance.getInFile();
        assertEquals(name, result);
    }

    /**
     * Test of setOutFile method, of class FileIO.
     */
    @Test
    public void testSetOutFile() {
        System.out.println("setOutFile");
        String name = "testFile";
        FileIO instance = new FileIO();
        instance.setOutFile(name);
        String result = instance.getOutFile();
        assertEquals(name, result);
    }

    /**
     * Test of printUnsolvablePuzzle method, of class FileIO.
     */
    @Test
    public void testPrintUnsolvablePuzzle() {
        System.out.println("printUnsolvablePuzzle");
        String inFile = "Input/Puzzle-4x4-0903.txt";
        String outFile = "Test/Unsolvable.txt";
        FileIO instance = new FileIO();
        instance.setInFile(testFileLocation + inFile);
        instance.setOutFile(testFileLocation + outFile);
        instance.printUnsolvablePuzzle();
        String expResult = "";
        String result = "";
        // Check to see if the results were what we expected
        try(Scanner scan = new Scanner(new FileReader(testFileLocation + inFile)))
        {
            while(scan.hasNextLine())
            {
                expResult += scan.nextLine() + "\n";
            }
            expResult += "Unsolvable\n";
        }catch (FileNotFoundException ex) {
            Logger.getLogger(FileIOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Check to see if the results were what we expected
        try(Scanner scan = new Scanner(new FileReader(testFileLocation + outFile)))
        {
            while(scan.hasNextLine())
            {
                result += scan.nextLine() + "\n";
            }
        }catch (FileNotFoundException ex) {
            Logger.getLogger(FileIOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(expResult, result);
    }

    /**
     * Test of getInFile method, of class FileIO.
     */
    @Test
    public void testGetInFile() {
        System.out.println("getInFile");
        FileIO instance = new FileIO();
        String expResult = "test";
        instance.setInFile(expResult);
        String result = instance.getInFile();
        assertEquals(expResult, result);
    }

    /**
     * Test of getOutFile method, of class FileIO.
     */
    @Test
    public void testGetOutFile() {
        System.out.println("getOutFile");
        FileIO instance = new FileIO();
        String expResult = "test";
        instance.setOutFile(expResult);
        String result = instance.getOutFile();
        assertEquals(expResult, result);
    }
    
}
