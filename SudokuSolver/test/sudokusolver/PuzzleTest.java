/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

import IO.FileIOTest;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class PuzzleTest {
    private static Puzzle testPuzzle;
    
    public PuzzleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        try {
            String values[] = {"1", "2", "3", "4" };
            testPuzzle = new Puzzle(4, values);
            testPuzzle.setCell(0, 0, "2");
            testPuzzle.setCell(0, 2, "3");
            testPuzzle.setCell(0, 3, "1");
            testPuzzle.setCell(1, 0, "1");
            testPuzzle.setCell(1, 1, "3");
            testPuzzle.setCell(1, 3, "4");
            testPuzzle.setCell(2, 0, "3");
            testPuzzle.setCell(2, 1, "1");
            testPuzzle.setCell(2, 2, "4");
            testPuzzle.setCell(3, 1, "2");
            testPuzzle.setCell(3, 2, "1");
            testPuzzle.setCell(3, 3, "3");
        } catch (BadInputException ex) {
            Logger.getLogger(FileIOTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAssignmentException ex) {
            Logger.getLogger(PuzzleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of cellSolved method, of class Puzzle.
     */
    @Test
    public void testCellSolvedTrue() {
        System.out.println("cellSolvedTrue");
        int row = 0;
        int column = 0;
        Puzzle instance = testPuzzle;
        boolean expResult = true;
        boolean result = instance.cellSolved(row, column);
        assertEquals(expResult, result);
    }
   
    /**
     * Test of cellSolved method, of class Puzzle.
     */
    @Test
    public void testCellSolvedFalse() {
        System.out.println("cellSolvedFalse");
        int row = 0;
        int column = 1;
        Puzzle instance = testPuzzle;
        boolean expResult = false;
        boolean result = instance.cellSolved(row, column);
        assertEquals(expResult, result);
    }

    /**
     * Test of setCell method, of class Puzzle.
     * @throws java.lang.Exception
     */
    @Test
    public void testSetCellValid() throws Exception {
        System.out.println("setCellValid");
        int row = 0;
        int column = 0;
        String value = "1";
        String values[] = {"1", "2", "3", "4" };
        Puzzle instance = new Puzzle(4, values);
        instance.setCell(row, column, value);
        List<String> expResult = new ArrayList();
        expResult.add("1");
        List<String> result = instance.getPossibleValues(row, column);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setCell method, of class Puzzle.
     * @throws java.lang.Exception
     */
    @Test
    public void testSetCellInvalidValue() throws Exception {
        System.out.println("setCellInvalidValue");
        int row = 0;
        int column = 0;
        String value = "7";
        String values[] = {"1", "2", "3", "4" };
        Puzzle instance = new Puzzle(4, values);
        try
        {
            instance.setCell(row, column, value);
        }
        catch (BadInputException ex)
        {
            // If the exception was thrown, it was not set as expected
            System.out.println("The BadInputException was thrown as expected");
        }
        List<String> expResult = new ArrayList();
        expResult.add("1");
        expResult.add("2");
        expResult.add("3");
        expResult.add("4");
        List<String> result = instance.getPossibleValues(row, column);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setCell method, of class Puzzle.
     * @throws java.lang.Exception
     */
    @Test
    public void testSetCellIllegalValue() throws Exception {
        System.out.println("setCellInvalidValue");
        int row = 0;
        int column = 1;
        String value = "1";
        String values[] = {"1", "2", "3", "4" };
        Puzzle instance = new Puzzle(4, values);
        try
        {
            instance.setCell(0, 0, "2");
            instance.setCell(0, 2, "3");
            instance.setCell(0, 3, "1");
            instance.setCell(row, column, value);
        }
        catch (IllegalAssignmentException ex)
        {
            // If the exception was thrown, it was not set as expected
            System.out.println("The IllegalAssignmentException was thrown as expected");
        }
        List<String> expResult = new ArrayList();
        expResult.add("4");
        List<String> result = instance.getPossibleValues(row, column);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of unsolvable method, of class Puzzle.
     */
    @Test
    public void testUnsolvableFalse() {
        System.out.println("unsolvableFalse");
        Puzzle instance = testPuzzle;
        boolean expResult = false;
        boolean result = instance.unsolvable();
        assertEquals(expResult, result);
    }

    /**
     * Test of unsolvable method, of class Puzzle.
     */
    @Test
    public void testUnsolvableTrue() {
        try {
            System.out.println("unsolvableTrue");
            String values[] = {"1", "2", "3", "4" };
            Puzzle instance = new Puzzle(4, values);
            instance.setCell(0, 0, "1");
            instance.setCell(0, 1, "2");
            instance.setCell(0, 2, "3");
            instance.setCell(1, 3, "4");
            boolean expResult = true;
            boolean result = instance.unsolvable();
            assertEquals(expResult, result);
        } catch (BadInputException | IllegalAssignmentException ex) {
            Logger.getLogger(PuzzleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of isSolved method, of class Puzzle.
     */
    @Test
    public void testIsSolvedFalse() {
        System.out.println("isSolvedFalse");
        String values[] = {"1", "2", "3", "4" };
        Puzzle instance = new Puzzle(4, values);
        boolean expResult = false;
        boolean result = instance.isSolved();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getPossibleValues method, of class Puzzle.
     */
    @Test
    public void testGetPossibleValues() {
        System.out.println("getPossibleValues");
        int row = 0;
        int column = 0;
        String values[] = {"1", "2", "3", "4" };
        Puzzle instance = new Puzzle(4, values);
        List<String> expResult = new ArrayList<>();
        expResult.add("1");
        expResult.add("2");
        expResult.add("3");
        expResult.add("4");
        List<String> result = instance.getPossibleValues(row, column);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Puzzle.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Puzzle instance = testPuzzle;
        String expResult = "4\n" + "1 2 3 4 \n" + "2 4 3 1 \n" +
                            "1 3 2 4 \n" + "3 1 4 2 \n" + "4 2 1 3 \n";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of print method, of class Puzzle.
     */
    @Test
    public void testPrint() {
        System.out.println("print");
        Puzzle instance = testPuzzle;
        instance.print();
    }

    /**
     * Test of isSolved method, of class Puzzle.
     */
    @Test
    public void testIsSolvedTrue() {
        try {
            System.out.println("isSolvedTrue");
            String values[] = {"1", "2", "3", "4" };
            Puzzle instance = new Puzzle(4, values);
            instance.setCell(0, 0, "2");
            instance.setCell(0, 1, "4");
            instance.setCell(0, 2, "3");
            instance.setCell(0, 3, "1");
            instance.setCell(1, 0, "1");
            instance.setCell(1, 1, "3");
            instance.setCell(1, 2, "2");
            instance.setCell(1, 3, "4");
            instance.setCell(2, 0, "3");
            instance.setCell(2, 1, "1");
            instance.setCell(2, 2, "4");
            instance.setCell(2, 3, "2");
            instance.setCell(3, 0, "4");
            instance.setCell(3, 1, "2");
            instance.setCell(3, 2, "1");
            instance.setCell(3, 3, "3");
            boolean expResult = true;
            boolean result = instance.isSolved();
            assertEquals(expResult, result);
        } catch (BadInputException ex) {
            Logger.getLogger(PuzzleTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAssignmentException ex) {
            Logger.getLogger(PuzzleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
