/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

import IO.FileIOTest;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class OnlyPossibleValueTest {
    private static Puzzle testPuzzle;
    private static String testFileLocation;
    private static Puzzle solvedPuzzle;
    
    public OnlyPossibleValueTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        try {
            testFileLocation = "/home/ammon/usu/ObjectOriented/Homework4/SudokuSolver/SamplePuzzles/";
            String values[] = {"1", "2", "3", "4" };
            // set up the test puzzle
            testPuzzle = new Puzzle(4, values);
            testPuzzle.setCell(0, 0, "2");
            testPuzzle.setCell(0, 2, "3");
            testPuzzle.setCell(0, 3, "1");
            testPuzzle.setCell(1, 0, "1");
            testPuzzle.setCell(1, 1, "3");
            testPuzzle.setCell(1, 3, "4");
            testPuzzle.setCell(2, 0, "3");
            testPuzzle.setCell(2, 1, "1");
            testPuzzle.setCell(2, 2, "4");
            testPuzzle.setCell(3, 1, "2");
            testPuzzle.setCell(3, 2, "1");
            testPuzzle.setCell(3, 3, "3");
            // Set up solved puzzle
            solvedPuzzle = new Puzzle(4, values);
            solvedPuzzle.setCell(0, 0, "2");
            solvedPuzzle.setCell(0, 1, "4");
            solvedPuzzle.setCell(0, 2, "3");
            solvedPuzzle.setCell(0, 3, "1");
            solvedPuzzle.setCell(1, 0, "1");
            solvedPuzzle.setCell(1, 1, "3");
            solvedPuzzle.setCell(1, 2, "2");
            solvedPuzzle.setCell(1, 3, "4");
            solvedPuzzle.setCell(2, 0, "3");
            solvedPuzzle.setCell(2, 1, "1");
            solvedPuzzle.setCell(2, 2, "4");
            solvedPuzzle.setCell(2, 3, "2");
            solvedPuzzle.setCell(3, 0, "4");
            solvedPuzzle.setCell(3, 1, "2");
            solvedPuzzle.setCell(3, 2, "1");
            solvedPuzzle.setCell(3, 3, "3");
        } catch (BadInputException | IllegalAssignmentException ex) {
            Logger.getLogger(FileIOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setPuzzle method, of class OnlyPossibleValue.
     */
    @Test
    public void testSetPuzzle() {
        System.out.println("setPuzzle");
        Puzzle p = testPuzzle;
        OnlyPossibleValue instance = new OnlyPossibleValue();
        instance.setPuzzle(p);
        assertEquals(p.toString(), instance.getPuzzle().toString());
    }

    /**
     * Test of solveSquares method, of class OnlyPossibleValue.
     */
    @Test
    public void testSolveSquares() throws Exception {
        System.out.println("solveSquares");
        OnlyPossibleValue instance = new OnlyPossibleValue();
            String values[] = {"1", "2", "3", "4" };
            // set up the test puzzle
            Puzzle toSolve = new Puzzle(4, values);
            toSolve.setCell(0, 0, "2");
            toSolve.setCell(0, 2, "3");
            toSolve.setCell(0, 3, "1");
            toSolve.setCell(1, 0, "1");
            toSolve.setCell(1, 1, "3");
            toSolve.setCell(1, 3, "4");
            toSolve.setCell(2, 0, "3");
            toSolve.setCell(2, 1, "1");
            toSolve.setCell(2, 2, "4");
            toSolve.setCell(3, 1, "2");
            toSolve.setCell(3, 2, "1");
            toSolve.setCell(3, 3, "3");
        instance.setPuzzle(toSolve);
        boolean expResult = true;
        boolean result = instance.solveSquares();
        assertEquals(expResult, result);
        assertEquals(toSolve.toString(), solvedPuzzle.toString());
        solvedPuzzle.print();
    }
    
}
