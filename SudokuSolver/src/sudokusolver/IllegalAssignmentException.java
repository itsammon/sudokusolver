/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

/**
 *
 * @author ammon
 */
public class IllegalAssignmentException extends Exception {

    public IllegalAssignmentException(String message) {
        super(message);
    }
    
}
