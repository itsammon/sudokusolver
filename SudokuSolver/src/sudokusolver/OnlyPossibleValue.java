/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

import java.util.List;

/**
 *
 * @author ammon
 */
public class OnlyPossibleValue extends SolvingAlgorithm{
    
    public OnlyPossibleValue()
    {
        super();
    }
    
    public OnlyPossibleValue(Puzzle p)
    {
        super(p);
    }
    
    @Override
    public boolean checkCell(int row, int column) throws BadInputException, IllegalAssignmentException {
        List<String> possibleValues;
        boolean changedValue = false;
        if (!puzzle.cellSolved(row, column)) {
            possibleValues = puzzle.getPossibleValues(row, column);
            if (possibleValues.size() == 1) {
                puzzle.setCell(row, column, possibleValues.get(0));
                changedValue = true;
            }
        }
        return changedValue;
    }
}
