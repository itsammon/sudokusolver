/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ammon
 */
public class MultipleSolutionsException extends Exception {
    private final List<Puzzle> puzzles;
    
    public MultipleSolutionsException(String m, List<Puzzle> puzzles)
    {
        super (m);
        this.puzzles = new ArrayList<>();
        for (Puzzle p: puzzles)
        {
            this.puzzles.add(p);
        }
    }
    
    public List<Puzzle> getSolutions()
    {
        return puzzles;
    }
}
