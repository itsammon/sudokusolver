/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

/**
 *
 * @author ammon
 */
public class BadInputException extends Exception {

    public BadInputException(String message) {
        super(message);
    }
    
}
