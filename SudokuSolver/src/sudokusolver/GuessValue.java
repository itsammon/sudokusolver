/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ammon
 */
public class GuessValue extends SolvingAlgorithm{
    private final Solver solver;
    private List<Puzzle> solvedPuzzles = new ArrayList<>();
    
    public GuessValue(Puzzle p, Solver s)
    {
        super(p);
        solver = s;
    }
    
    @Override
    public boolean solveSquares() throws BadInputException, IllegalAssignmentException, MultipleSolutionsException
    {
        boolean changedValue = false;
        int row = 0, column = 0;
        // Large number to get the first row/column
        int possibleValues = 200;
        
        // Loop through to find an unsolved cell
        for (int i = 0; i < puzzle.size(); ++i)
        {
            for (int j = 0; j < puzzle.size(); ++j)
            {
                if (!puzzle.cellSolved(i, j) && puzzle.getPossibleValues(i, j).size() < possibleValues)
                {
                    row = i;
                    column = j;
                    possibleValues = puzzle.getPossibleValues(row, column).size();
                }
            }
        }
        if (checkCell(row, column))
        {
            changedValue = true;
        }
        return changedValue;
    }

    @Override
    public boolean checkCell(int row, int column) throws BadInputException, IllegalAssignmentException, MultipleSolutionsException {
        List<String> possibleValues = puzzle.getPossibleValues(row, column);
        boolean foundSolution = false;
        if (!puzzle.cellSolved(row, column))
        {
            for (String value : possibleValues) {
                Puzzle guessPuzzle = new Puzzle(puzzle);
                guessPuzzle.setCell(row, column, value);
                Puzzle result = solver.solvePuzzle(guessPuzzle);
                if (result.isSolved()) {
                    solvedPuzzles.add(result);
                    if (foundSolution)
                    {
                        for (Puzzle p: solvedPuzzles)
                            if (!result.toString().equals(p.toString()))
                            {
                                throw new MultipleSolutionsException("Multiple solutions found!", solvedPuzzles);
                            }
                            else
                            {
                                solvedPuzzles.remove(solvedPuzzles.size()-1);
                            }
                    }
                    foundSolution = true;
                }
            }
        }
        // We need to set the puzzle to the solved puzzle when we are done
        if (solvedPuzzles.size() >= 1)
        {
            puzzle = solvedPuzzles.get(0);
        }
        return foundSolution;
    }
    
}
