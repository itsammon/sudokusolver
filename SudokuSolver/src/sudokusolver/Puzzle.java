/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ammon
 */
public class Puzzle {
    private final List<String>[][] grid;
    private final boolean [][] solvedValues;
    private final int size;
    private final List<String> possible;
    private final int n;
    
    public Puzzle(int size, String[] possibleValues)
    {
        this.size = size;
        this.n = (int)Math.sqrt((double)size);
        this.possible = new ArrayList<>();
        // Add the list of possible values
        for (String s: possibleValues)
        {
            possible.add(s);
        }
        grid = new ArrayList[size][size];
        solvedValues = new boolean[size][size];
        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
            {
                grid[i][j] = new ArrayList<>(possible);
                solvedValues[i][j] = false;
            }
        }
    }
    
    public Puzzle(int size, List<String> possibleValues)
    {
        this.size = size;
        this.n = (int)Math.sqrt((double)size);
        this.possible = new ArrayList<>(possibleValues);
        grid = new ArrayList[size][size];
        solvedValues = new boolean[size][size];
        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
            {
                grid[i][j] = new ArrayList<>(possible);
                solvedValues[i][j] = false;
            }
        }
    }
    
    public Puzzle(Puzzle p)
    {
        this.size = p.size();
        this.n = p.n;
        this.possible = p.possible;
        grid = new ArrayList[size][size];
        solvedValues = new boolean[size][size];
        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
            {
                grid[i][j] = new ArrayList<>(p.getPossibleValues(i, j));
                // Set the cells to the appropriate values
                solvedValues[i][j] = p.cellSolved(i, j);
            }
        }
    }
    
    public int size()
    {
        return size;
    }
    
    public boolean cellSolved(int row, int column)
    {
        if (row >= 0 && column >= 0 && row < size && column < size)
        {
            return solvedValues[row][column];
        }
        else
        {
            // May want to throw an outofbounds exception or something similar
            return false;
        }
    }
    
    public void setCell(int row, int column, String value) throws BadInputException, IllegalAssignmentException
    {
        if (!possible.contains(value))
            throw new BadInputException(value + " to be inserted at (" + row + "," + column 
                                            + ") is not a possible value!");
        if (grid[row][column].contains(value))
        {
            // Empty the cell
            grid[row][column].clear();
            
            // Eliminate the value from the possible values of the other cells
            for (int i = 0; i < size; ++i)
            {
                if (i != column)
                {
                    grid[row][i].remove(value);
                }
                if (i != row)
                {
                    grid[i][column].remove(value);
                }
            }
            // Remove it from possible options in the block
            int blockRow = (row/n) * n;
            int blockColumn = (column/n) * n;
            for (int x = blockRow; x < blockRow+n; ++x)
            {
                for (int y = blockColumn; y < blockColumn+n; ++y)
                {
                    grid[x][y].remove(value);
                }
            }
            // Set the row and column to that value
            grid[row][column].add(value);
            solvedValues[row][column] = true;
        }
        else
        {
            //TODO: Throw an exception as the value cannot be set.
            throw new IllegalAssignmentException("The value " + value + 
                                " is illegal in cell (" + row + "," + column + ")");
        }
    }
    
    public boolean unsolvable()
    {
        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
            {
                if (grid[i][j].size() < 1)
                    // We hava a cell with no possible values in it.
                    return true;
            }
        }
        return false;
    }
    
    public boolean isSolved()
    {
        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
            {
                if (solvedValues[i][j] == false)
                    return false;
            }
        }
        return true;
    }
    
    public List<String> getPossibleValues(int row, int column)
    {
        return grid[row][column];
    }
    
    @Override
    public String toString()
    {
        // List the puzzle size
        String string = size + "\n";
        
        // List the possible values
        for (String s: possible)
        {
            string += s + " ";
        }
        string += "\n";
        
        // Add each value from the puzzle
        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
            {
                if (grid[i][j].size() > 0)
                {
                    // If the values are not solved, list them in curly braces
                    if (!solvedValues[i][j])
                    {
                        string += "{";
                    }
                    for (String s: grid[i][j])
                    {
                        string += s;
                    }
                    if (!solvedValues[i][j])
                    {
                        string += "}";
                    }
                }
                else
                {
                    string += "-";
                }
                string += " ";
            }
            string += "\n";
        }
        return string;
    }    
    
    // Allow quick printing of the puzzle
    public void print()
    {
        System.out.print(toString());
    }
}
