/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

/**
 *
 * @author ammon
 */
public abstract class SolvingAlgorithm {
    protected Puzzle puzzle;
    
    public SolvingAlgorithm()
    {
        puzzle = null;
    }
    
    public SolvingAlgorithm(Puzzle p)
    {
        puzzle = p;
    }
    
    public void setPuzzle(Puzzle p)
    {
        puzzle = p;
    }
    
    public Puzzle getPuzzle()
    {
        return puzzle;
    }
    
    public boolean solveSquares() throws BadInputException, IllegalAssignmentException, MultipleSolutionsException
    {
        boolean changedValues = false;
        
        // Loop through each cell
        for (int i = 0; i < puzzle.size(); ++i)
        {
            for (int j = 0; j < puzzle.size(); ++j)
            {
                if (!puzzle.cellSolved(i, j))
                {
                    if (checkCell(i, j))
                    {
                        changedValues = true;
                    }
                }
            }
        }
        return changedValues;
    }
    
    public abstract boolean checkCell(int row, int column) throws BadInputException,
                            IllegalAssignmentException, MultipleSolutionsException;
}
