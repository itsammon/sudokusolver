/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

/**
 *
 * @author ammon
 */
public class Solver {
    
    public Puzzle solvePuzzle(Puzzle puzzle) throws BadInputException, BadInputException, IllegalAssignmentException, MultipleSolutionsException
    {
        SolvingAlgorithm algorithm;
        boolean solved = true;
        boolean solving = true;
        boolean updated = true;
        algorithm = new OnlyPossibleValue(puzzle);
        while (solving)
        {
            while (algorithm.solveSquares())
            {
                updated = true;
                
                if (algorithm.getPuzzle().isSolved()) {
                    solving = false;
                    solved = true;
                    break;
                }
                if (algorithm.getPuzzle().unsolvable())
                {
                    solving = false;
                    solved = false;
                    break;
                }
            }
            if (solving == false)
                break;
            
            // Pick the algorithm appropriate to solve the puzzle
            if (algorithm.getClass() == OnlyPossibleValue.class && updated == true)
            {
                algorithm = new OnlyValidCell(puzzle);
                updated = false;
            }
            else if(algorithm.getClass() == OnlyValidCell.class)
            {
                algorithm = new OnlyPossibleValue(puzzle);
                updated = false;
            }
            else if (algorithm.getClass() == GuessValue.class)
            {
                solving = false;
            }
            else
            {
                algorithm = new GuessValue(puzzle, this);
                updated = false;
            }
        }
        return algorithm.getPuzzle();
    }
}
