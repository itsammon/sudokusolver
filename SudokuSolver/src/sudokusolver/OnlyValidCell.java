/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

import java.util.List;

/**
 *
 * @author ammon
 */
public class OnlyValidCell extends SolvingAlgorithm{
    
    public OnlyValidCell()
    {
        super();
    }
    
    public OnlyValidCell(Puzzle p)
    {
        super(p);
    }
    
    @Override
    public boolean checkCell(int row, int column) throws BadInputException, IllegalAssignmentException {
        if (puzzle == null) {
            // May change this to an exception.
            return false;
        }
        List<String> possibleValuesToCheck;
        boolean changedValue = false;
        if (!puzzle.cellSolved(row, column)) {
            possibleValuesToCheck = puzzle.getPossibleValues(row, column);
            // For each value check if it can only be placed in that cell
            for (String val : possibleValuesToCheck) {
                
                // Check the column
                if (checkColumn(row, column, val)) {
                    // We found that the value can only be in that cell for the column
                    puzzle.setCell(row, column, val);
                    changedValue = true;
                    break;
                }
                
                // Check the row
                if (checkRow(row, column, val)) {
                    // We found that the value can only be in that cell for the column
                    puzzle.setCell(row, column, val);
                    changedValue = true;
                    break;
                }
                
                // Check the block
                if (checkBlock(row, column, val)){
                    // We found that the value can only be in that cell for the column
                    puzzle.setCell(row, column, val);
                    changedValue = true;
                    break;
                }
            }
        }
        return changedValue;
    }
    
    private boolean checkColumn(int row, int column, String val)
    {
        List<String> possibleValues;
        // Flag so we can set things appropriately
        boolean isOnly = true;
        
        // Loop through the column
        for (int i = 0; i < puzzle.size(); ++i) {
            // The puzzle will take care of values in solved cells
            if (!puzzle.cellSolved(i, column) && i != row) {
                possibleValues = puzzle.getPossibleValues(i, column);
                if (possibleValues.contains(val)) {
                    // Set the isOnly flag to false and exit the loop
                    isOnly = false;
                    break;
                }
            }
        }
        return isOnly;
    }
    
    private boolean checkRow(int row, int column, String val)
    {
        List<String> possibleValues;
        // Flag so we can set things appropriately
        boolean isOnly = true;
        
        // Loop through the column
        for (int i = 0; i < puzzle.size(); ++i) {
            // The puzzle will take care of values in solved cells
            if (!puzzle.cellSolved(row, i) && i != column) {
                possibleValues = puzzle.getPossibleValues(row, i);
                if (possibleValues.contains(val)) {
                    // Set the isOnly flag to false and exit the loop as we don't need to check any more cells for the value
                    isOnly = false;
                    break;
                }
            }
        }
        return isOnly;
    }
    
    private boolean checkBlock(int row, int column, String val)
    {
        List<String> possibleValues;
        // Flag so we can set things appropriately
        boolean isOnly = true;
        int n = (int)Math.sqrt((double)puzzle.size());
        
        // Remove it from possible options in the block
        int blockRow = (row/n) * n;
        int blockColumn = (column/n) * n;
        for (int x = blockRow; x < blockRow+n; ++x)
        {
            for (int y = blockColumn; y < blockColumn+n; ++y)
            {
                // Don't check the current square
                if (x != row || y != column)
                {
                    possibleValues = puzzle.getPossibleValues(x, y);
                    if (possibleValues.contains(val)) {
                        // Set the isOnly flag to false and exit the loop as we don't need to check any more cells for the value
                        isOnly = false;
                        break;
                    }
                }
            }
            // Break out of the outer loop if the value is not unique
            if (!isOnly)
            {
                break;
            }
        }
        // Return if it is the only valid value
        return isOnly;
    }
}
