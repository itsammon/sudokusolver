/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import sudokusolver.BadInputException;
import sudokusolver.IllegalAssignmentException;
import sudokusolver.Puzzle;

/**
 *
 * @author ammon
 */
public class FileIO {
    private static List<Integer> validSizes = new ArrayList<>();
    private String inFileName;
    private String outFileName;
    
    public FileIO()
    {
        validSizes.add(4);
        validSizes.add(9);
        validSizes.add(16);
        validSizes.add(25);
        validSizes.add(36);
    }
    
    /**
     * Gets the currently set input file
     * @return The input file path
     */
    public String getInFile()
    {
        return inFileName;
    }
    
    /**
     * Set the current input file
     * @param name The input file path
     */
    public void setInFile(String name)
    {
        inFileName = name;
    }
    
    /**
     * Gets the currently set output file
     * @return The output file path
     */
    public String getOutFile()
    {
        return outFileName;
    }
    
    /**
     * Set the output file
     * @param name The file path
     */
    public void setOutFile(String name)
    {
        outFileName = name;
    }
    
    /**
     * Load in a puzzle from the input file
     * @return The puzzle resulting or null if there was an issue
     * Prints out bad puzzles if there was an exception
     * @throws FileNotFoundException File was not found
     */
    public Puzzle load() throws FileNotFoundException
    {
        Puzzle puzzle = null;
        int size;
        String line;
        String symbols[];
        String values[];
        try(Scanner scan = new Scanner(new FileReader(inFileName)))
        {
            // Get the size
            size = scan.nextInt();
            if (!validSizes.contains(size))
            {
                throw new BadPuzzleException("Invalid puzzle size!");
            }
            // Get the rest of the line
            scan.nextLine();
            
            line = scan.nextLine();
            symbols = line.split("\\s+");
            if (symbols.length != size)
            {
                throw new BadPuzzleException("Puzzle formatted incorrectly!");
            }
            
            puzzle = new Puzzle(size, symbols);
            
            // Loop through the rows
            for (int i = 0; i < size; ++i)
            {
                line = scan.nextLine();
                values = line.split("\\s+");
                // Print out bad puzzle
                if (values.length != size)
                {
                    throw new BadPuzzleException("Puzzle formatted incorrectly!");
                }
                
                // Loop through the columns
                for (int j = 0; j < size; ++j)
                {
                    // If the cell is not supposed to be blank add in the value
                    if (!values[j].equals("-"))
                    {
                        // Throws a BadInputException if the value is not correct
                        // Throws a IllegalAssignmentException if the value is illegal
                        puzzle.setCell(i, j, values[j]);
                    }
                }
            }
               
        }catch (IOException ex) {
            // Something went wrong with the FileIO
            // TODO: Decide what to do if something went wrong
            // Do I throw and exception or do I print something out.
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }catch (BadPuzzleException | BadInputException ex)
        {
            // If the puzzle was formatted incorrectly or
            // had values that were not in the symbol list,
            // print out the bad puzzle
            puzzle = null;
            printBadPuzzle();
        }catch (IllegalAssignmentException ex)
        {
            // If there is an illegal value in the initial puzzle, print it back out as unsolvable
            puzzle = null;
            printUnsolvablePuzzle();
        }
        
        return puzzle;
    }   
        
    /**
     * Write a puzzle out to the outFile
     * @param p The puzzle to write out
     */
    public void print(Puzzle p)
    {
        try(PrintWriter writer = new PrintWriter(new FileWriter(outFileName)))
        {
            writer.println(p.toString());
            
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Write out the bad puzzle with "Bad Puzzle" beneath it
     * @throws FileNotFoundException 
     */
    private void printBadPuzzle() throws FileNotFoundException
    {
        String line;
        try(Scanner scan = new Scanner(new FileReader(inFileName)))
        {
            PrintWriter writer = new PrintWriter(new FileWriter(outFileName));
            while (scan.hasNextLine())
            {
                line = scan.nextLine();
                writer.println(line);
            }
            writer.println("Bad Puzzle");
            
            writer.close();
            
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Print out the unsolvable puzzle with the line "Unsolvable" beneath it
     */
    public void printUnsolvablePuzzle()
    {
        String line;
        try(Scanner scan = new Scanner(new FileReader(inFileName)))
        {
            PrintWriter writer = new PrintWriter(new FileWriter(outFileName));
            while (scan.hasNextLine())
            {
                line = scan.nextLine();
                writer.println(line);
            }
            writer.println("Unsolvable");
            
            writer.close();
            
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Print out the puzzle with the line "MultipleSolutions" beneath it
     * Print out each solution found
     * @param solutions The solved puzzles of each solution
     */
    public void printMultipleSolutions(List<Puzzle> solutions)
    {
        String line;
        try(Scanner scan = new Scanner(new FileReader(inFileName)))
        {
            PrintWriter writer = new PrintWriter(new FileWriter(outFileName));
            while (scan.hasNextLine())
            {
                line = scan.nextLine();
                writer.println(line);
            }
            writer.println("MultipleSolutions");
            
            for (Puzzle s: solutions)
            {
                writer.println(s.toString());
            }
            
            writer.close();
            
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
