/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO;

/**
 *
 * @author ammon
 */
public class BadPuzzleException extends Exception{
    
    public BadPuzzleException(String message)
    {
        super(message);
    }
}
