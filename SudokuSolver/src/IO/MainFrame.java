/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO;

import IO.FileIO;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import javax.swing.BoxLayout;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import sudokusolver.BadInputException;
import sudokusolver.IllegalAssignmentException;
import sudokusolver.MultipleSolutionsException;
import sudokusolver.Puzzle;
import sudokusolver.Solver;

/**
 *
 * @author ammon
 */
public class MainFrame extends JFrame{                    
    private javax.swing.JButton inputFileButton;
    private javax.swing.JButton outputFileButton;
    private javax.swing.JButton solveButton;
    private javax.swing.JLabel inputLabel;
    private javax.swing.JLabel outputLabel;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JScrollPane scrollpane;
    private javax.swing.JSplitPane inputSplitPane;
    private javax.swing.JSplitPane outputSplitPane;
    private javax.swing.JTextArea programOutput;
    private javax.swing.JTextField inputFilePathTextField;
    private javax.swing.JTextField outputFileTextField;
    
    private boolean inputSet = false;
    private boolean outputSet = false;
    // End of variables declaration
    
    public MainFrame(String name)
    {
        super(name);
        // Set minimum size and behaviors
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(100, 100));
        setSize(new Dimension(500,500));
        
        // Set up the content pane
        setLayout(new BorderLayout());
        getContentPane().setSize(500, 500);
        getContentPane().setLayout(new BorderLayout());
        
         // Set the loacation of the frame
        initComponents();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        inputSplitPane = new javax.swing.JSplitPane();
        inputLabel = new javax.swing.JLabel();
        inputFilePathTextField = new javax.swing.JTextField();
        outputSplitPane = new javax.swing.JSplitPane();
        outputLabel = new javax.swing.JLabel();
        outputFileTextField = new javax.swing.JTextField();
        scrollpane = new javax.swing.JScrollPane();
        programOutput = new javax.swing.JTextArea();
        buttonPanel = new javax.swing.JPanel();
        inputFileButton = new javax.swing.JButton();
        outputFileButton = new javax.swing.JButton();
        solveButton = new javax.swing.JButton();
        
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        
        inputLabel.setText("Input Location");
        inputSplitPane.setLeftComponent(inputLabel);

        inputFilePathTextField.setEditable(false);
        inputFilePathTextField.setText("");
        inputFilePathTextField.setName("InputFileName"); // NOI18git N
        inputFilePathTextField.setPreferredSize(new java.awt.Dimension(250, 27));
        inputSplitPane.setRightComponent(inputFilePathTextField);

        inputSplitPane.setName("InputLabel"); // NOI18N
        
        mainPanel.add(inputSplitPane);

        outputLabel.setText("Output Location");
        outputLabel.setName("OutputLabel"); // NOI18N
        outputSplitPane.setLeftComponent(outputLabel);

        outputFileTextField.setEditable(false);
        outputFileTextField.setText("");
        outputFileTextField.setName("OutputFileName"); // NOI18N
        outputFileTextField.setPreferredSize(new java.awt.Dimension(250, 27));
        outputSplitPane.setRightComponent(outputFileTextField);

        mainPanel.add(outputSplitPane);

        programOutput.setEditable(false);
        programOutput.setColumns(60);
        programOutput.setRows(25);
        scrollpane.setViewportView(programOutput);

        mainPanel.add(scrollpane);

        add(mainPanel, java.awt.BorderLayout.CENTER);

        inputFileButton.setText("Set Input Location");
        inputFileButton.setName("inputFileButton"); // NOI18N
        buttonPanel.add(inputFileButton);
        inputFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputFileActionPerformed(evt);
            }
        });

        outputFileButton.setText("Set Output Location");
        outputFileButton.setName("outputFileButton"); // NOI18N
        buttonPanel.add(outputFileButton);
        outputFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputFileActionPerformed(evt);
            }
        });

        solveButton.setText("Solve Puzzle(s)");
        solveButton.setName("solveButton"); // NOI18N
        buttonPanel.add(solveButton);
        solveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                solveActionPerformed(evt);
            }
        });

        add(buttonPanel, java.awt.BorderLayout.SOUTH);

        pack();
    }
    
    private void solvePuzzles()
    {
        FileIO file = new FileIO();
        try (Stream<Path> paths = Files.walk(Paths.get(inputFilePathTextField.getText()))) {
            paths.forEach(filePath -> {
                Puzzle puzzle;
                if (Files.isRegularFile(filePath)) {
                    file.setInFile(filePath.toString());
                    file.setOutFile(outputFileTextField.getText() + "/" + filePath.getFileName().toString());
                    
                    try {
                        programOutput.append("Solving " + file.getInFile() + "\n");
                        puzzle = file.load();
                        if (puzzle != null)
                        {
                            solvePuzzle(puzzle, file);
                        }
                        programOutput.append("Result written to " + file.getOutFile() + "\n");
                        
                    } catch (FileNotFoundException | BadInputException | IllegalAssignmentException ex) {
                        programOutput.append(ex.getMessage() + "\n");
                    }
                    
                }
            });
        } catch (IOException ex) {
            programOutput.append(ex.getMessage() + "\n");
        }
    }
    
    private static void solvePuzzle(Puzzle puzzle, FileIO io) throws BadInputException, BadInputException, IllegalAssignmentException
    {
        Solver solver = new Solver();
        Puzzle result;
        try {
            result = solver.solvePuzzle(puzzle);
            if (result.isSolved()) {
                io.print(result);
            } else if (result.unsolvable()) {
                io.printUnsolvablePuzzle();
            }
        } catch (MultipleSolutionsException ex) {
            io.printMultipleSolutions(ex.getSolutions());
        }
    }

    private void inputFileActionPerformed(ActionEvent evt) {
        //Create a file chooser
        final JFileChooser chooser = new JFileChooser();
        File workingDirectory = new File(System.getProperty("user.dir"));
        chooser.setCurrentDirectory(workingDirectory);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        
        // Get whether they canceled out of the choser
        int returnVal = chooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            //This is where a real application would open the file.
            inputFilePathTextField.setText(file.getAbsolutePath());
            inputSet = true;
        } else {
            programOutput.append("Command cancelled by user.\n");
        }
    }
    
    
    private void outputFileActionPerformed(ActionEvent evt) {
        //Create a file chooser and set it to the working directory
        final JFileChooser chooser = new JFileChooser();
        File workingDirectory = new File(System.getProperty("user.dir"));
        chooser.setCurrentDirectory(workingDirectory);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        
        // Get whether they canceled out of it or not
        int returnVal = chooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            //This is where a real application would open the file.
            outputFileTextField.setText(file.getAbsolutePath());
            outputSet = true;
        } else {
            programOutput.append("Command cancelled by user.\n");
        }
    }
    
    private void solveActionPerformed(ActionEvent evt) 
    {
        if (!inputSet)
        {
            programOutput.append("No input location set!\n");
        }
        else if (!outputSet)
        {
            programOutput.append("No output location set!\n");
        }
        else
        {
            solvePuzzles();
        }
    }
}
